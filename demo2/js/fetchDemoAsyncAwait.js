const ulElement = document.getElementById('abilities');
console.log('LOG 1');

const response = await fetch('https://pokeapi.co/api/v2/pokemon/ditto');

let object;
if (response.ok) {
    console.log('LOG FROM RESPONSE HANDLER');
    object = await response.json();
} else {
    throw new Error('Response not OK ...');
}

console.log('LOG 2');

console.log('LOG FROM OBJECT HANDLER');
object.abilities.forEach(obj => {
    console.log('Ability name: ' + obj.ability.name);
    const liElement = document.createElement('li');
    liElement.innerText = obj.ability.name;
    ulElement.appendChild(liElement);
});

console.log('LOG 3');
