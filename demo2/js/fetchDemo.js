const ulElement = document.getElementById('abilities');
console.log('LOG 1');

fetch('https://pokeapi.co/api/v2/pokemon/ditto')
    .then(handleResponse)
    .then(handleObject);

/*const promise1 = fetch('https://pokeapi.co/api/v2/pokemon/ditto');
const promise2 = promise1.then(handleResponse);
promise2.then(handleObject);*/

console.log('LOG 2');

function handleObject(object) { // Asynchronously
    console.log('LOG FROM OBJECT HANDLER');
    object.abilities.forEach(obj => {
        console.log("Ability name: " + obj.ability.name);
        const liElement = document.createElement('li');
        liElement.innerText = obj.ability.name;
        ulElement.appendChild(liElement);
    });
}

/**
 * @param {Response} response
 */
function handleResponse(response) {
    if (response.ok) {
        console.log('LOG FROM RESPONSE HANDLER');
        return response.json();
    }
    throw new Error("Response not OK ...");
}

console.log('LOG 3');
