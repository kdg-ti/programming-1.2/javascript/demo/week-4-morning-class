const inputArea = document.getElementById('input');
const outputArea = document.getElementById('output');
const button = document.getElementsByTagName('button')[0];

button.addEventListener('click', processInput);

function processInput() {
    /**
     * @type {String}
     */
    const text = inputArea.value;

    const pattern = /[A-Z]{2,3}[0-9]{3,4}/g;
    //const pattern = /([A-Z]{2,3})([0-9]{3,4})/g;
    // Method of RegExp: 'test'
    // const found = pattern.test(text);

    // Method of String: 'matchAll'
    let result = '';
    const matches = text.matchAll(pattern);
    for (const match of matches) {
        result += match[0] + '\n';
        console.log(match);
        //result += '  group 1: ' + match[1] + '\n';
        //result += '  group 2: ' + match[2] + '\n';
    }

    outputArea.value = result;
}
